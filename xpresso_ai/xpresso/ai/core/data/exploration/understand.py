from xpresso.ai.core.data.exploration import Explorer
from sklearn.base import BaseEstimator

class Understander(BaseEstimator):
    """
    Extracting attribute type for the Structured Dataset object.
    """
    def fit(self,dataset):
        """
        Extracts type of each attribute
        Args:
            dataset (StructuredDataset): StructuredDataset object
        """
        explorer = Explorer(dataset)
        explorer.understand()