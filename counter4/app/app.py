"""
This is the implementation of data preparation for sklearn using Xpresso
abstract pipeline component
"""
import os
import sys

__author__ = "Naveen Sinha"


from xpresso.ai.core.data.pipeline.abstract_pipeline_component import \
    AbstractPipelineComponent


class Counter(AbstractPipelineComponent):
    """ Counter
  """

    def __init__(self, c3):
        super().__init__(name="Counter 4")
        self.final_val = int(c3)
        self.initial_val = 1
        print("Counter initialization completed:{} {}", self.initial_val, self.final_val )

    def start(self, run_name):
        super().start(run_name)
        print("Started count")
        for count in range (self.initial_val, self.final_val):
            print (count)
            self.send_metrics(count)
        self.completed(push_exp=False)

    def send_metrics(self, count):
        report_status = {"status": {"status": "Counter 3"},
                         "metric": {"count": count}
                        }
        if not self.local_execution:
            self.report_status.report_pipeline_status(status=report_status)


    def completed(self, push_exp=True):
        output_dir = "/data/kaggle_dataset"
        #super().completed(push_exp=push_exp)


if __name__ == "__main__":
    counter = Counter(sys.argv[1])
    counter.start(run_name='test_1_6')
