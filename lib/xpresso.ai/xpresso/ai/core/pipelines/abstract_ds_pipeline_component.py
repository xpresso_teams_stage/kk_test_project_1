__all__=["AbstractDSPipelineComponent"]
__author__ = "Srijan Sharma"

from xpresso.ai.core.pipelines.abstract_pipeline_component import AbstractPipelineComponent

class AbstractDSPipelineComponent(AbstractPipelineComponent):
    """
        Abstract Classs for all Data Science Components
    """
    def  __init__(self,data=None, state=None, results=None, ispause=None):
        """

        Args:
            data: Data used by component for working
            state: State of the Component
            results: Results to be persisted by component
            ispause: Flag defining if the component is paused or not
        """
        AbstractPipelineComponent.__init__(self,data, state, results, ispause)