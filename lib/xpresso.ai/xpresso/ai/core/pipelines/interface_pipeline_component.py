__all__=["InterfacePipelineComponent"]
__author__ = "Srijan Sharma"

from abc import ABC, abstractmethod


class InterfacePipelineComponent(ABC):
    """
    This interface represents a generic pipelines component.
    """
    def __init__(self):
        super().__init__()

    @abstractmethod
    def start(self, *args):
        """
        Starts the component’s execution
        Returns:

        """
        pass

    @abstractmethod
    def terminate(self, *args):
        """
        Starts the component’s execution
        Returns:

        """
        pass

    @abstractmethod
    def pause(self, *args):
        """
        Starts the component’s execution
        Returns:

        """
        pass

    @abstractmethod
    def restart(self, *args):
        """
        Starts the component’s execution
        Returns:

        """
        pass


