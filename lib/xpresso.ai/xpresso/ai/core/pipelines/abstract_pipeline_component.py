__all__=["AbstractPipelineComponent"]
__author__ = "Srijan Sharma"

from xpresso.ai.core.pipelines.interface_pipeline_component import InterfacePipelineComponent
from abc import  abstractmethod

class AbstractPipelineComponent(InterfacePipelineComponent):
    """
    This is an abstract implementation of the PipelineComponent interface.
    """
    def __init__(self, name=None):
        """

        Args:
            name: component name
        """
        super().__init__()
        self.name = name

        # any data used by component
        self.data = None
        # component state (to be saved during pause / loaded during restart)
        self.state = None
        # component results
        self.results = None
        # run ID
        self.run_id = None
        return

    def start(self, *args):
        """
        default implementation of start method (may be overwritten by sub-class)
        """
        # note down run ID
        self.run_id = args[1]

    def terminate(self):
        """
        default implementation of start method (may be overwritten by sub-class)
        """
        exit(0)

    def pause(self):
        """
        default implementation of pause method (may be overwritten by sub-class)
        """
        # save state to disk
        exit(0)

    def completed(self):
        pass

    def status(self, status):
        pass

    def restarted(self):
        pass

    def load_state(self):
        pass
