__all__=["AbstractEstimator"]
__author__ = "Srijan Sharma"

from xpresso.ai.core.pipelines.abstract_ds_pipeline_component import AbstractDSPipelineComponent
from abc import  abstractmethod

class AbstractEstimator(AbstractDSPipelineComponent):
    """
    Abstract class representing an Estimator component
    """
    def  __init__(self, state, results):
        """

        Args:
            state: Dictionary storing state of the estimator
            results: Final Results to be persisted
        """
        AbstractDSPipelineComponent.__init__(self,state=state,results=results)

    def start(self):
        """
        Entry point to this component
        Returns:

        """
        self.fit()
        return

    @abstractmethod
    def fit(self):
        """
        Fits the model on the data
        Returns:

        """
        pass