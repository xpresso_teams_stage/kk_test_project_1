__all__ = ['VersioningAuthenticator']
__author__ = 'Gopi Krishna'

from xpresso.ai.core.commons.utils.xpr_config_parser import XprConfigParser
from xpresso.ai.core.commons.network.http.send_request import SendHTTPRequest
from xpresso.ai.core.commons.network.http.http_request import HTTPMethod
from xpresso.ai.core.commons.exceptions.xpr_exceptions \
    import AuthenticationFailedException, \
    ControllerClientResponseException, InvalidEnvironmentException
from xpresso.ai.core.commons.utils.constants import \
     config_paths, DV_INPUT_UID, DV_INPUT_ENV, DV_INPUT_PASSWORD
from xpresso.ai.client.context_manager import UserContextManager


class VersioningAuthenticator:
    """
    Authenticates the requests for using Data versioning module
    """
    CONTROLLER_SECTION = 'controller'
    SERVER_URL = 'server_url'
    TOKEN = "token"
    CONTEXT_KEY_IN_LOGIN = "user_context"

    def __init__(self, **credentials):
        """
        Class constructor
        :param credentials:
        """
        self.check_credentials(**credentials)
        self.config_path = XprConfigParser.DEFAULT_CONFIG_PATH
        self.config = self.fetch_config(**credentials)
        self.validate_credentials(**credentials)

    def check_credentials(self, **credentials):
        """
        checks if the valid credentials are provided or not
        :param credentials:
        :keyword Argument:
            uid:
                uid of the user
            pwd:
                password of the user
            env:
                workspace to be used
        """
        if self.TOKEN not in credentials:
            mandatory_fields = [
                DV_INPUT_UID,
                DV_INPUT_PASSWORD,
                DV_INPUT_ENV
            ]
            for field in mandatory_fields:
                if field not in credentials:
                    raise AuthenticationFailedException(
                        f"{field} field not provided"
                    )
            if credentials[DV_INPUT_ENV] not in config_paths:
                raise InvalidEnvironmentException(
                    f"invalid workspace value:"
                    f" {credentials[DV_INPUT_ENV]}"
                )

    def fetch_config(self, **credentials):
        """
        fetches the config values as per user entrance point to
         PachydermVersionController
        :param credentials:
        :return:
            return a dict with config values
        """
        env_str = credentials[DV_INPUT_ENV]
        path_to_config = config_paths[env_str]
        self.config_path = path_to_config
        return XprConfigParser(path_to_config)

    def check_login(self, **credentials):
        """
        check if the login credentials are valid or not
        :param credentials:
        :keyword Arguments:
            uid: str:
                uid of the user
            pwd: str:
                password of the user
        :return:
            returns the response from login operation
        """
        server_endpoint = self.config[self.CONTROLLER_SECTION][self.SERVER_URL]
        url = f"{server_endpoint}/auth"
        login_response = SendHTTPRequest().send(
            url=url, http_method=HTTPMethod.POST,
            data={
                "uid": credentials[DV_INPUT_UID],
                "pwd": credentials[DV_INPUT_PASSWORD]
            }
        )
        return login_response

    def check_token(self, **credentials):
        """
        checks if the token is valid or not

        :param credentials:
        :keyword Argument:
            token:
                login token saved on user system
        :return:
        """
        server_endpoint = self.config[self.CONTROLLER_SECTION][self.SERVER_URL]
        url = f"{server_endpoint}/versioning/auth"
        token_response = SendHTTPRequest().send(
            url=url, http_method=HTTPMethod.GET,
            header={"token": credentials[self.TOKEN]},
            data={}
        )
        return token_response

    def validate_credentials(self, **credentials):
        """
        validates the provided credentials

        :param credentials:
        """
        if self.TOKEN in credentials:
            response = self.check_token(**credentials)
        else:
            response = self.check_login(**credentials)
        UserContextManager().load_context(response[self.CONTEXT_KEY_IN_LOGIN])
