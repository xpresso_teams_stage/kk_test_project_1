import abc


class VersionControllerInterface(metaclass=abc.ABCMeta):
    """"""
    def __init__(self):
        self.controller_name = None

    @abc.abstractmethod
    def create_repo(self, repo_info):
        """"""

    @abc.abstractmethod
    def list_repo(self):
        """"""

    @abc.abstractmethod
    def create_branch(self, repo_info):
        """"""

    @abc.abstractmethod
    def list_branch(self):
        """"""

    @abc.abstractmethod
    def push_dataset(self):
        """"""

    @abc.abstractmethod
    def pull_dataset(self):
        """"""

    @abc.abstractmethod
    def list_dataset(self):
        """"""
