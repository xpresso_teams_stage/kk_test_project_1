from xpresso.ai.core.data.versioning.versioning_authenticator \
    import VersioningAuthenticator
from xpresso.ai.core.data.versioning.pachyderm_controller import \
    PachydermVersionController


class VersionControllerFactory:
    """"""
    PACHYDERM = "pachyderm"
    VERSIONING_CONFIG = "data_versioning"
    TOOL = "tool"
    SERVER = "server"
    HOST = "cluster_ip"
    PORT = "port"

    def __init__(self, **credentials):
        """"""
        auth = VersioningAuthenticator(**credentials)
        self.versioning_tool = auth.config[self.VERSIONING_CONFIG][self.TOOL]
        self.version_controller_info = self.controller_info(auth.config)

    def get_version_controller(self):
        """"""
        if self.versioning_tool == self.PACHYDERM:
            return PachydermVersionController(self.version_controller_info)

    def controller_info(self, config):
        """"""
        if self.versioning_tool == self.PACHYDERM:
            return {
                "name": self.versioning_tool,
                "host": config[self.VERSIONING_CONFIG][self.SERVER][self.HOST],
                "port": config[self.VERSIONING_CONFIG][self.SERVER][self.PORT]
            }