__all__ = ["AttributeInfo"]
__author__ = "Srijan Sharma"

from xpresso.ai.core.logging.xpr_log import XprLogger
from xpresso.ai.core.data.exploration.explore_numeric import ExploreNumeric
from xpresso.ai.core.data.exploration.explore_date import ExploreDate
from xpresso.ai.core.data.exploration.explore_categorical import ExploreCategory
from xpresso.ai.core.data.exploration.explore_text import ExploreText
from xpresso.ai.core.data.exploration.explore_string import ExploreString
from xpresso.ai.core.data.automl.dataset_type import DECIMAL_PRECISION
from xpresso.ai.core.commons.utils.constants import DEFAULT_PROBABILITY_BINS


class AttributeInfo:

    def __init__(self, attribute_name):
        self.logger = XprLogger()
        self.name = attribute_name
        self.metrics = dict()

    def populate(self, data, threshold, bins=DEFAULT_PROBABILITY_BINS):

        if str(self.type) == "numeric":
            self.metrics = ExploreNumeric(data, threshold,
                                          probability_dist_bins=bins).populate_numeric()

        elif str(self.type) == "ordinal" or self.type == "nominal":
            self.metrics = ExploreCategory(data).populate_category()

        elif str(self.type) == "date":
            self.metrics = ExploreDate(data).populate_date()

        elif str(self.type) == "string":
            self.metrics = ExploreString(data).populate_string()

        elif str(self.type) == "text":
            self.metrics = ExploreText(data).populate_text()

        na_count, na_count_percentage, missing_count, \
        missing_count_percentage = self.na_analysis(
            data)
        self.metrics["na_count"] = na_count
        self.metrics["na_count_percentage"] = na_count_percentage
        self.metrics["missing_count"] = missing_count
        self.metrics["missing_count_percentage"] = missing_count_percentage

    @staticmethod
    def na_analysis(data):

        num_rows = float(data.size)
        na_count = float(data.isna().sum())

        na_count_percentage = round((na_count / num_rows) * 100,
                                    DECIMAL_PRECISION)
        missing_count = float((data == "").sum())
        missing_count_percentage = round((missing_count / num_rows) * 100,
                                         DECIMAL_PRECISION)

        return na_count, na_count_percentage, missing_count, \
               missing_count_percentage
