__all__ = ["ReportStatus"]
__author__ = "Srijan Sharma"

import os

from xpresso.ai.core.commons.utils.singleton import Singleton
from xpresso.ai.core.commons.utils.constants import ENABLE_LOCAL_EXECUTION
from xpresso.ai.core.data.pipeline.pipeline_component_controller import PipelineController

class ReportStatus(metaclass=Singleton):
    def __init__(self,component_name=None,xpresso_run_name=None):
        self.name = component_name
        self.xpresso_run_name = xpresso_run_name
        return

    def report_pipeline_status(self, status):
        print("Parent component reporting status")
        if os.environ.get(ENABLE_LOCAL_EXECUTION) is None or  \
                os.environ.get(ENABLE_LOCAL_EXECUTION) is False:
            PipelineController().report_pipeline_status(self.xpresso_run_name,
                                                        self.name,status)
        else:
            print(status)
