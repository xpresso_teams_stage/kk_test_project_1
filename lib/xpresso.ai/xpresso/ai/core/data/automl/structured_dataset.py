""" Class design for Dataset"""

import pandas as pd

from xpresso.ai.core.data.automl.dataset import AbstractDataset
from xpresso.ai.core.data.automl.dataset_type import DatasetType
from xpresso.ai.core.data.exploration.data_type import DataType
from xpresso.ai.core.data.automl import utility
from xpresso.ai.core.logging.xpr_log import XprLogger
from xpresso.ai.core.data.exploration.structured_dataset_info import \
    StructuredDatasetInfo
from xpresso.ai.core.data.connections import Connector
from IPython.display import display
import csvdiff
import os

__all__ = ['StructuredDataset']
__author__ = 'Srijan Sharma'

# This is indented as logger can not be serialized and can not be part
# of automl
logger = XprLogger()


class StructuredDataset(AbstractDataset):
    """ StructuredDataset stores the data in tabular format. It reads data
    from csv, excel or any database. It stores the automl into local storage
    in pickle format."""

    def __init__(self, dataset_name: str = "default_dataset",
                 project_name: str="default_project",
                 created_by: str = "default",
                 description: str = "This is a structured automl"):
        super().__init__(dataset_name=dataset_name,
                         description=description,
                         project_name=project_name,
                         created_by=created_by)

        self.info = StructuredDatasetInfo()
        self.type = DatasetType.STRUCTURED

    def import_dataset(self, user_config, local_storage_required: bool = False,
                       sample_percentage: float = 100):

        """ Fetches automl from multiple data sources and loads them
        into a automl"""

        self.data = Connector().getconnector(
            user_datasource=user_config.get("type"),
            datasource_type=user_config.get("data_source")).import_dataframe(
            user_config)

        self.local_storage_required = local_storage_required
        self.sample_percentage = sample_percentage

    def save(self):
        """ Save the automl into the local file system in
        a serialized format

        Returns:
            str: folder path where serialized data has been stored
        """
        serialized_data = self.serialize()
        new_pickle_file_name = self.get_pickle_file_path()
        with open(new_pickle_file_name, "wb") as pickle_fs:
            pickle_fs.write(serialized_data)
        return new_pickle_file_name

    def load(self, pickle_file_name=None):
        """
        Load the automl from the local file system
        in a serialized format

        Args:
            pickle_file_name: name of the exact folder where pickles are
            present.
                              if not, it will pick from default directory

        Returns:
            bool: True if load is successful, False otherwise.
        """
        if not pickle_file_name:
            pickle_file_name = self.get_pickle_file_path()
        with open(pickle_file_name, "rb") as pickle_fs:
            serialized_data = pickle_fs.read()
            dataset_obj = self.deserialize(serialized_data)
            self.import_from_dataset(dataset_obj)
            return True

    def diff(self, new, output_path=None):
        """ Finds the difference between two automl class"""
        metadata_diff = self.compare_metadata(self.info.attributeInfo,
                                              new.info.attributeInfo)
        data_diff = self.compare_data(self.data, new.data)
        self.output_diff(metadata_diff, data_diff, new, output_path)
        pass

    def output_diff(self, metadata_diff, data_diff, new, output_path=None):
        """helper function output difference between automl to excel file
        Args:
            metadata_diff('list'): list of tuple of the difference in metadata
            of two datasets
            data_diff('list'): Rows added, removed and changed in new automl
            
            """
        old = self
        if output_path is None:
            output_path = "./data_difference"
        data_diff_filename = 'data_difference.xlsx'
        modification_type = ["added", "removed", "changed"]
        sheets = dict()
        for ittr in modification_type:
            rows_data = list()
            rows_data.append(["Old Dataset: {}".format(old.name)])
            rows_data.append(["New Dataset: {}".format(new.name)])
            rows_data.append([])
            rows_data.append([])

            diff_data = data_diff[ittr]
            if len(diff_data) > 0:
                rows_data.append(list(diff_data[0].keys()))
                for data in diff_data:
                    rows_data.append(list(data.values()))
                rows_data = pd.DataFrame(rows_data)
            data_df = pd.DataFrame(rows_data)
            sheets[ittr] = data_df
        utility.to_excel(output_path, data_diff_filename, sheets)

        metadata_diff_filename = "metadata_difference.csv"
        if len(metadata_diff) > 0:
            df = list()
            for data in metadata_diff:
                df.append([data[0], data[1]])
            df = pd.DataFrame(df, columns=["Atrribute", "Modification_type"])
            df.to_csv(os.path.join(output_path, metadata_diff_filename),
                      header=True, index=False)

    @staticmethod
    def compare_metadata(old, new):
        """
        Compares the metadata of two automl classes i.e. attributeInfo for
        each automl is compared
        """
        identical = True
        metadata_old = list()
        metadata_new = list()
        difference = list()

        for attr in new:
            metadata_new.append((attr.name, attr.dtype, attr.type))

        for attr in old:
            metadata_old.append((attr.name, attr.dtype, attr.type))

        metadata_diff = list(set(metadata_old).symmetric_difference(set(
            metadata_new)))

        for attr_diff in metadata_diff:

            if attr_diff in metadata_old:
                name = attr_diff[0]
                type = attr_diff[2]

                # If the name is present in the old attributeinfo, but not in
                # the new one
                if name not in [attr[0] for attr in metadata_new]:
                    print("{} has been removed.Not found in the latest "
                          "version".format(attr_diff))
                    difference.append((name, "removed"))
                    identical = False

                # if the attribute corresponding to that name is present in
                # old and new attributeinfo, but only the type has changed
                for attr_new in metadata_new:
                    if name is attr_new[0] and type is not attr_new[2]:
                        print("Type of {} changed from {} to {} in the "
                              "latest version".format(name, type, attr_new[2]))
                        difference.append((name, "updated"))
                        identical = False
                        break

            # if the attribute is present in latest version but not in the old
            # version
            elif attr_diff in metadata_new:
                name = attr_diff[0]
                print("{} added in the latest version".format(attr_diff))
                difference.append((name, "added"))
                identical = False

        if identical:
            print("Metadata for both versions identical")
        return difference

    @staticmethod
    def compare_data(old, new):
        """
        Compares the pandas dataframe of two automl classes
        """
        new = new.dropna()
        old = old.dropna()

        new['id'] = new.apply(lambda x: hash(tuple(x)), axis=1)
        old['id'] = old.apply(lambda x: hash(tuple(x)), axis=1)
        old_records = old.to_dict("records")
        new_records = new.to_dict("records")
        data_diff = csvdiff.diff_records(old_records, new_records, ['id'])
        return data_diff

    def unique(self, attr_name=None, top=None):
        """Function to get top n unique categories
        of a categorical attribute
        Args:
            attr_name('str'): Attribute name of which unique category to be
            listed
            top('int'): Number of top categories to be listed"""
        if attr_name is None:
            print("provide attribute name")
            return
        for attr in self.info.attributeInfo:
            if attr.name != attr_name:
                continue
            if attr.type not in [DataType.NOMINAL.value,
                                 DataType.ORDINAL.value]:
                print("{} is not a categorical attribute".format(attr_name))
                return
            unique = attr.metrics["freq_count"]
            unique = pd.Series(unique).to_frame()
            if top is not None:
                unique = unique.head(top)
            display(unique)

    def filter(self, items=None, like=None, axis=None, regex=None):
        """Filters the automl into subset
        items('list'): Keep labels from axis which are in items.
        like('string'): Keep labels from axis for which “like in label == True”.
        regex('string'): Keep labels from axis for which re.search(regex,
        label) == True.
        axis('int'): The axis to filter on. By default
        this is ‘columns’ for DataFrame.
        """
        self.data = self.data.filter(items=items, like=like, axis=axis,
                                     regex=regex)

    def change_type(self, attribute_name, new_type):
        """Change the type of an attribute
        Args:
            attribute_name('str'): attribute whose type is to be changed
            new_type('str'): New type t assigned to the attribute"""
        attr_list = list(filter(lambda attr: attr.name == attribute_name,
                                self.info.attributeInfo))
        if not attr_list:
            print("{} attribute doesn't exist".format(attribute_name))
            return
        attr = attr_list[0]
        if new_type == DataType.NUMERIC.value and attr.type == \
                DataType.NOMINAL.value and \
                (attr.dtype == DataType.FLOAT.value
                 or attr.dtype == DataType.INT.value):
            attr.type = new_type
        elif new_type == DataType.NOMINAL.value and (
                attr.type == DataType.NUMERIC.value or
                attr.type == DataType.TEXT.value):
            attr.type = new_type
        elif new_type == DataType.TEXT.value and (
                attr.type == DataType.NOMINAL.value or
                attr.dtype == DataType.OBJECT.value):
            attr.type = new_type

        if attr.type != new_type:
            print("{} to {} not possible for {} attribute".format(attr.type,
                                                                  new_type,
                                                                  attr.name))

    def show(self, k=5):
        """Displays first and last k rows of the dataframe
        Args:
            k (int): Number of rows to be shown
        """
        if not len(self.data):
            # print is required,  to display on the message on the user side
            print("Unable to display empty data frame")
        print("Top {} rows".format(k))
        display(self.data.head(k))
        print("\n Last {} rows".format(k))
        display(self.data.tail(k))
